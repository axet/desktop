package com.github.axet.desktop.os.linux;

import java.util.Map;

import org.freedesktop.dbus.connections.impl.DBusConnection;
import org.freedesktop.dbus.connections.impl.DBusConnection.DBusBusType;
import org.freedesktop.dbus.types.DBusListType;
import org.freedesktop.dbus.types.UInt32;
import org.freedesktop.dbus.types.Variant;
import org.gnome.Mutter.DisplayConfig;
import org.gnome.Mutter.Quadruple;

public class LinuxDisplay {

	public static final String BUS_NAME = "org.gnome.Mutter.DisplayConfig";
	public static final String OBJECT_PATH = "/org/gnome/Mutter/DisplayConfig";
	public static final String LEGACY_UI_SCALING_FACTOR = "legacy-ui-scaling-factor";

	public static int getScale() {
		DBusConnection conn = null;
		try {
			conn = DBusConnection.getConnection(DBusBusType.SESSION);
			DisplayConfig config = (DisplayConfig) conn.getRemoteObject(BUS_NAME, OBJECT_PATH, DisplayConfig.class);
			Quadruple<UInt32, DBusListType, DBusListType, Map<String, Variant>> o = config.GetCurrentState();
			for (String k : o.d.keySet()) {
				Variant v = o.d.get(k);
				if (k.equals(LEGACY_UI_SCALING_FACTOR))
					return ((Variant<Integer>) v).getValue();
			}
		} catch (RuntimeException e) {
			throw e;
		} catch (Exception e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null)
				conn.disconnect();
		}
		return 0;
	}

}
