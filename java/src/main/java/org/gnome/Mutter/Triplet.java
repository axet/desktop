package org.gnome.Mutter;

import org.freedesktop.dbus.Tuple;

public class Triplet<A, B, C> extends Tuple {
	public final A a;
	public final B b;
	public final C c;

	public Triplet(A a, B b, C c) {
		this.a = a;
		this.b = b;
		this.c = c;
	}
}
