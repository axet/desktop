package org.gnome.Mutter;

import java.util.Map;

import org.freedesktop.dbus.interfaces.DBusInterface;
import org.freedesktop.dbus.types.DBusListType;
import org.freedesktop.dbus.types.UInt32;
import org.freedesktop.dbus.types.Variant;

// https://gitlab.gnome.org/GNOME/mutter/blob/master/src/org.gnome.Mutter.DisplayConfig.xml

public interface DisplayConfig extends DBusInterface {
	public Quadruple<UInt32, DBusListType, DBusListType, Map<String, Variant>> GetCurrentState();
}
