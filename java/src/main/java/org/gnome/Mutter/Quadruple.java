package org.gnome.Mutter;

import org.freedesktop.dbus.Tuple;

public class Quadruple<A, B, C, D> extends Tuple {
	public final A a;
	public final B b;
	public final C c;
	public final D d;

	public Quadruple(A a, B b, C c, D d) {
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
	}
}
